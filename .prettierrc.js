module.exports = {
  semi: false,
  singleQuote: true,
  proseWrap: 'never',
}
