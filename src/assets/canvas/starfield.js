import polyfillRAF from './requestAnimationFrame';

if (process.browser) polyfillRAF();

let ctx;
let reqId;
let stars;
let star;
const cursor = { x: 0, y: 0 };

const params = {
  width: 0,
  height: 0,
  halfWidth: 0,
  halfHeight: 0,
  depth: 0,
  ratX: 0,
  ratY: 0,
  test: true
};

let settings = {
  speed: 1,
  density: 512,
  ratio: 256,
  bgColor: '#292f6d',
  starColor: '#fff',
  loading: true
};

function init(canvas) {
  ctx = canvas.getContext('2d');
  setSize();

  stars = [];
  for (let i = 0; i < settings.density; i++) {
    stars.push({
      x: Math.random() * params.width * 2 - params.width,
      y: Math.random() * params.height * 2 - params.height,
      z: Math.round(Math.random() * params.depth),
      dx: 0,
      dy: 0,
      dxSave: 0,
      dySave: 0
    });

    ctx.fillStyle = settings.bgColor;
    ctx.strokeStyle = settings.starColor;
  }
}

function setSize() {
  params.width = window.innerWidth;
  params.height = window.innerHeight;

  params.ratX = params.width / ctx.canvas.width;
  params.ratY = params.height / ctx.canvas.height;

  ctx.canvas.width = params.width;
  ctx.canvas.height = params.height;

  params.halfWidth = Math.round(params.width / 2);
  params.halfHeight = Math.round(params.height / 2);
  params.depth = params.halfWidth + params.halfHeight;
}

function animate() {
  ctx.fillRect(0, 0, params.width, params.height);

  for (let i = 0; i < settings.density; i++) {
    star = stars[i];
    params.test = true;
    star.dxSave = star.dx;
    star.dySave = star.dy;

    star.x += cursor.x >> 7;
    if (star.x > params.width) {
      star.x -= params.width << 1;
      params.test = false;
    }
    if (star.x < -params.width) {
      star.x += params.width << 1;
      params.test = false;
    }

    star.y += cursor.y >> 7;
    if (star.y > params.height) {
      star.y -= params.height << 1;
      params.test = false;
    }
    if (star.y < -params.height) {
      star.y += params.height << 1;
      params.test = false;
    }

    star.z -= settings.speed;
    if (star.z > params.depth) {
      star.z -= params.depth;
      params.test = false;
    }
    if (star.z < 0) {
      star.z += params.depth;
      params.test = false;
    }

    star.dx = params.halfWidth + (star.x / star.z) * settings.ratio;
    star.dy = params.halfHeight + (star.y / star.z) * settings.ratio;

    if (
      star.dxSave > 0 &&
      star.dxSave < params.width &&
      star.dySave > 0 &&
      star.dySave < params.height &&
      params.test
    ) {
      ctx.lineWidth = (1 - star.z / params.depth) * 2;
      ctx.beginPath();
      ctx.moveTo(star.dxSave, star.dySave);
      ctx.lineTo(star.dx, star.dy);
      ctx.stroke();
      ctx.closePath();
    }
  }

  reqId = requestAnimationFrame(animate);
}

function onResize() {
  setSize();

  // Recalculate the position of each star proportionally to new w and h
  stars.forEach(star => {
    star.x *= params.ratX;
    star.y *= params.ratY;
    star.dx = params.halfWidth + (star.x / star.z) * settings.ratio;
    star.dy = params.halfHeight + (star.y / star.z) * settings.ratio;
  });

  ctx.fillStyle = settings.bgColor;
  ctx.strokeStyle = settings.starColor;
}

function onOrientation(event) {
  if (event.beta == null && event.gamma == null) return;

  let x;
  let y;
  if (params.width > params.height) {
    x = event.beta * -1;
    y = event.gamma;
  } else {
    x = event.gamma;
    y = event.beta;
  }
  cursor.x = params.width / 2 + x * 5 - params.halfWidth;
  cursor.y = params.height / 2 + y * 5 - params.halfHeight;
}

function onMousemove(event) {
  cursor.x = event.clientX - params.halfWidth;
  cursor.y = event.clientY - params.halfHeight;
}

function start() {
  window.addEventListener('resize', onResize);
  window.addEventListener('orientationchange', onResize);
  window.addEventListener('deviceorientation', onOrientation);
  window.addEventListener('mousemove', onMousemove);

  animate();
}

function stop() {
  window.cancelAnimationFrame(reqId);

  window.removeEventListener('resize', onResize);
  window.removeEventListener('orientationchange', onResize);
  window.removeEventListener('deviceorientation', onOrientation);
  window.removeEventListener('mousemove', onMousemove);

  cursor.x = 0;
  cursor.y = 0;

  settings = {
    speed: 1,
    density: 512,
    ratio: 256,
    bgColor: '#292f6d',
    starColor: '#fff'
  };
}

export default {
  settings,
  init,
  start,
  animate,
  stop
};
