import {
  MeshStandardMaterial,
  Mesh,
  Group,
  SphereGeometry,
  OctahedronGeometry
} from 'three';

let orbit;
let rocketBody;
let rocketRadius;
let rocket;
let color;
const rocketFlame = [];
const rocketFlameDist = 30;
const flameColors = [
  0x2d237c,
  0x5b47f9,
  0x8c7efa,
  0xada3fc,
  0xdedafd,
  0xffffff
];

function draw() {
  const material = new MeshStandardMaterial({
    color: 0x32ffc8,
    roughness: 1,
    flatShading: true
  });

  orbit = new Mesh(new OctahedronGeometry(1, 0), material);

  rocket = new Group();
  rocket.position.x = -120;
  rocket.scale.set(1.3, 1.3, 1.3);
  orbit.add(rocket);

  rocketBody = new Mesh(new SphereGeometry(5, 4, 5), material);
  rocketBody.scale.y = 1.4;
  rocket.add(rocketBody);
  rocketRadius = rocketBody.geometry.parameters.radius;

  const wingMaterial = material.clone();
  wingMaterial.color.setHex(0x7e88fc);
  const wing1 = new Mesh(new OctahedronGeometry(2, 0), wingMaterial);
  wing1.scale.y = 1.2;
  wing1.position.y = -rocketBody.geometry.parameters.radius * 1.3;
  wing1.position.x = -rocketBody.geometry.parameters.radius * 0.8;
  rocket.add(wing1);

  const wing2 = wing1.clone();
  wing2.position.x = -wing1.position.x;
  rocket.add(wing2);

  const wing3 = wing1.clone();
  wing3.position.x = 0;
  wing3.position.z = -rocketBody.geometry.parameters.radius * 0.8;
  wing3.rotation.y = Math.PI / 2;
  rocket.add(wing3);

  const wing4 = wing3.clone();
  wing4.position.z = -wing3.position.z;
  rocket.add(wing4);

  const flameGeometry = new OctahedronGeometry(0.5, 1);

  for (let i = 0, flame; i < 30; i++) {
    flame = new Mesh(flameGeometry, material.clone());
    color = flameColors[i % 6];
    flame.material.color.setHex(color);
    rocketFlame.push(flame);
    rocket.add(flame);
  }

  return orbit;
}

let posY;

function animate() {
  orbit.rotation.z -= 0.01;
  orbit.rotation.y -= 0.001;
  rocket.rotation.y -= 0.005;
  rocketFlame.forEach((flame, i) => {
    if (getDistance(flame, rocketBody) < rocketFlameDist) {
      flame.position.y -= 0.2 * (i + 1);
      flame.position.x += Math.random() - 0.5;
      flame.position.z += Math.random() - 0.5;
    } else {
      flame.position.y =
        Math.random() * (rocketFlameDist - rocketRadius) - rocketFlameDist;
      posY = -flame.position.y * 0.03;
      flame.position.x =
        (Math.random() * rocketRadius - rocketRadius * 0.5) * posY;
      flame.position.z =
        (Math.random() * rocketRadius - rocketRadius * 0.5) * posY;
    }
    color = flameColors[-Math.round(flame.position.y / 5)];
    if (color) flame.material.color.setHex(color);
    flame.scale.x = flame.scale.y = flame.scale.z =
      getDistance(flame, rocketBody) * 0.05;
  });
}

function getDistance(mesh1, mesh2) {
  const dx = mesh1.position.x - mesh2.position.x;
  const dy = mesh1.position.y - mesh2.position.y;
  const dz = mesh1.position.z - mesh2.position.z;
  return Math.sqrt(dx * dx + dy * dy + dz * dz);
}

export default {
  draw,
  animate
};
