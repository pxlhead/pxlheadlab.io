import {
  Scene,
  PerspectiveCamera,
  WebGLRenderer,
  PCFSoftShadowMap,
  JSONLoader,
  DirectionalLight,
  PointLight,
  BufferGeometry,
  BufferAttribute,
  PointsMaterial,
  Points,
  Mesh,
} from 'three'
import { TweenMax, Power3 } from 'gsap'
import rocket from './rocket'

let isAnimating = false

let scene
let camera
let renderer

let planet
let rocketOrbit
let stars

let width
let height

const planetData = [
  { x: 0, y: 0, z: 0 },
  { x: 0, y: 0, z: 0 },
  { x: 1.9, y: 0.5, z: -3.4 },
  { x: -2, y: -1, z: 0 },
  { x: 0.6, y: 0.3, z: 1 },
]
const cameraData = [
  {
    position: { x: 0, y: 10, z: 200 },
    rotation: { x: 0, y: 0, z: 0 },
  },
  {
    position: { x: 50, y: 20, z: 120 },
    rotation: { x: 0.5, y: 0.2 },
  },
  {
    position: { x: 0, y: 65, z: 40 },
    rotation: { x: 0.1, y: -1 },
  },
  {
    position: { x: 40, y: 70, z: 100 },
    rotation: { x: 0, y: 0 },
  },
  {
    position: { x: 40, y: 90, z: 50 },
    rotation: { x: -0.3, y: 0 },
  },
]

function draw() {
  scene = new Scene()

  rocketOrbit = rocket.draw()
  rocketOrbit.position.set(0, 60, 350)
  scene.add(rocketOrbit)

  const jsonLoader = new JSONLoader()
  jsonLoader.load('/planet.json', (geometry, materials) => {
    // draw main planet
    planet = new Mesh(geometry, materials)
    planet.castShadow = true
    planet.receiveShadow = true
    planet.position.set(50, -10, 0)
    scene.add(planet)
  })

  const starGeometry = new BufferGeometry()
  const starPositions = new Float32Array(200 * 3)
  const starMaterial = new PointsMaterial({
    color: 0xa7a1ed,
    size: 1.3,
    sizeAttenuation: false,
  })
  for (let i = 0; i < 200; i++) {
    const randPos = () => Math.random() * 400 - 200
    starPositions[i * 3] = randPos() * 1.5
    starPositions[i * 3 + 1] = randPos()
    starPositions[i * 3 + 2] = randPos()
  }
  starGeometry.addAttribute('position', new BufferAttribute(starPositions, 3))
  stars = new Points(starGeometry, starMaterial)
  scene.add(stars)

  // draw lights
  const light1 = new DirectionalLight(0xadfffd, 0.8)
  light1.position.set(-80, 50, 200)
  scene.add(light1)

  const light2 = new PointLight(0xfaa0ff)
  light2.position.set(-80, 10, 180)
  scene.add(light2)
}

function init() {
  width = window.innerWidth
  height = window.innerHeight

  camera = new PerspectiveCamera(75, width / height, 0.1, 1000)
  camera.position.set(0, 60, 350)
  camera.rotation.x = Math.PI / 2
  camera.rotation.z = -Math.PI / 4

  renderer = new WebGLRenderer({ antialias: true, alpha: true })
  renderer.setPixelRatio(window.devicePixelRatio)
  renderer.setSize(width, height)
  renderer.shadowMap.enabled = true
  renderer.shadowMap.type = PCFSoftShadowMap

  window.addEventListener('resize', onResize)

  return renderer.domElement
}

function start() {
  isAnimating = true
  animate()
  TweenMax.to(camera.position, 5, {
    z: 200,
    y: 10,
    delay: 2,
    ease: Power3.easeOut,
  })
  TweenMax.to(camera.rotation, 5, {
    x: 0,
    y: 0,
    z: 0,
    delay: 2,
    ease: Power3.easeOut,
  })
  TweenMax.to(rocketOrbit.position, 5, {
    x: 50,
    y: -10,
    z: 0,
    delay: 2,
    ease: Power3.easeOut,
  })
}

function animate() {
  if (!isAnimating) return
  if (planet) {
    planet.rotation.y += 0.0001
    stars.rotation.y += 0.0005

    rocket.animate()
  }
  renderer.render(scene, camera)
  requestAnimationFrame(animate)
}

function onResize() {
  width = window.innerWidth
  height = window.innerHeight

  camera.aspect = width / height
  camera.updateProjectionMatrix()
  renderer.setSize(width, height)
}

function moveTargets(index) {
  TweenMax.to(camera.position, 2, {
    ...cameraData[index].position,
    ease: Power3.easeOut,
  })
  TweenMax.to(camera.rotation, 2, {
    ...cameraData[index].rotation,
    ease: Power3.easeOut,
  })
  TweenMax.to(planet.rotation, 2, {
    ...planetData[index],
    ease: Power3.easeOut,
  })
}

function stop() {
  isAnimating = false
  window.removeEventListener('resize', onResize)
}

export default {
  draw,
  init,
  start,
  stop,
  moveTargets,
}
